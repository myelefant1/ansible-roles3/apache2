## Apache2

This role installs and configures apache2.

Tested on debian 10.

## Role parameters

| name                        | value    | optionnal | default value   | description                              |
| ----------------------------|----------|-----------|-----------------|------------------------------------------|
| apache2_packages            | list     | yes       | []              | installed OS package apache modules      |
| apache2_modules_enabled     | list     | yes       | []              | enabled apache modules                   |
| apache2_modules_disabled    | list     | yes       | []              | disabled apache modules                  |
| apache2_conf_files          | list     | yes       | []              | configuration files                      |
| apache2_mods_files          | list     | yes       | []              | modules configuration files              |
| apache2_sites_files         | list     | yes       | []              | vhost configuration files                |
| apache2_sites_only          | boolean  | yes       | no              | vhost configuration files not in 'apache2_sites_files' are to be disabled |
| apache2_default_permission  | number   | yes       | 0600            | permissions of configuration files       |
| apache2_should_be_started   | boolean  | yes       | yes             | when true, the handlers will be executedand apache is started |

## Using this role

### ansible galaxy

Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/apache2.git
  scm: git
  version: v1.5
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: apache2
      apache2_packages: [libapache2-mod-proxy-uwsgi]
      apache2_modules_enabled:
        - cache
        - deflate
        - expires
      apache2_modules_disabled:
        - mpm_prefork
```

## Tests

[tests/tests_apache2](tests/tests_apache2)
