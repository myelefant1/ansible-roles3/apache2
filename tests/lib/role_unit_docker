#    (c) 2017-2016, n0vember <n0vember@half-9.net>
#
#    This file is part of role_unit.
#
#    role_unit is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    role_unit is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with role_unit.  If not, see <http://www.gnu.org/licenses/>.

ru_env_cnx="docker"
ru_env_docker_repo="registry.gitlab.com/role_unit/role_unit_containers"

### User environment

[ -n "$RU_ENV_DOCKER_REPO" ] && ru_env_docker_repo="$RU_ENV_DOCKER_REPO"
ru_fixed_names=${RU_FIXED_NAMES:-0}

### Global functions

declare -p ru_servers_names >/dev/null 2>&1 || declare -A ru_servers_names

ru_host_needs_privileged() {
  local ru_issue_file="${RU_ISSUE_FILE:-/etc/issue}"
  local ret_code=1
  if [ -f "${ru_issue_file}" ] ; then
    if grep ^Ubuntu ${ru_issue_file} >/dev/null ; then
      ret_code=0
    fi
    if grep ^Debian ${ru_issue_file} >/dev/null ; then
      ret_code=0
    fi
  fi
  return ${ret_code}
}

ru_destroy() {
  local ru_server
  for ru_server in $(ru_server_nums)
  do
    local ru_server_name=${ru_servers_names[${ru_server}]}
    ru_info stopping container $(ru_server ${ru_server})
    docker rm -f ${ru_server_name} >/dev/null 2>&1
  done
}

ru_pull() {
  local ru_image_name=$1
  local ru_image_repo=$(echo ${ru_image_name} | cut -d ":" -f 1)
  local ru_image_tag=$(echo ${ru_image_name} | cut -d ":" -f 2)

  ru_info pulling docker image
  docker pull ${ru_image_name} >/dev/null 2>&1

  ru_image_exists=$(docker images | grep "^${ru_image_repo} *${ru_image_tag} " | wc -l)
  [ ${ru_image_exists} -eq 0 ] && ru_usage "unable to pull image (${ru_image_name})"
}

ru_init() {
  local ru_server ru_server_name
  local ru_capacity="--cap-add=SYS_ADMIN"
  ru_host_needs_privileged && ru_capacity="--privileged"
  ru_dir_init
  if [ ${ru_debug} -eq 0 -o -z "${ru_conf}" ] ; then
    echo "RU_ENV_IMAGE=${ru_env_image}" >> ${ru_conf_file}
    ru_pull ${ru_env_docker_repo}:${ru_env_image}
    for ru_server in $(ru_server_nums)
    do
      [ ${ru_fixed_names} -eq 0 ] && ru_server_name=$(ru_uuid) || ru_server_name=${ru_env_name}_${ru_server}
      ru_info starting container $(ru_server ${ru_server})
      docker run \
        -d \
        ${ru_capacity} \
        --rm \
        --tmpfs /tmp \
        --tmpfs /run \
        --cgroupns=host \
        --cgroup-parent=docker.slice \
        -v /sys/fs/cgroup:/sys/fs/cgroup:rw \
        --name=${ru_server_name} \
        --hostname=${ru_server_name} \
        ${ru_env_docker_repo}:${ru_env_image} \
        >/dev/null 2>&1
      echo "ru_servers_names[${ru_server}]=${ru_server_name}" >> ${ru_conf_file}
    done
  fi
  . ${ru_conf_file}
  if [ ${ru_debug} -ne 1 ] ; then
    trap "ru_destroy" EXIT
  fi
  ru_ansible_init
  if [ ${ru_debug} -eq 1 ] ; then
    ru_info Debug mode activated. Environment will not be dropped after the tests.
    if [ ${ru_first_debug} -eq 1 ] ; then
      ru_info "this is the first run in this debug environment, set the variables before re-run:"
      ru_info "    export RU_DEBUG=1"
      ru_info "    export RU_CONF=${ru_conf_file}"
    fi
    ru_info to re-run the tests on the same environment, re-run the same command:
    ru_info "    $0 ${test_file}"
    ru_info to connect to containers, run the following:
    for ru_server in $(ru_server_nums)
    do
      ru_info "    docker exec -ti ${ru_servers_names[${ru_server}]} bash # $(ru_server ${ru_server})"
    done
    ru_info at the end, you will have to drop those containers manually and unset vars:
    ru_info "    $(ru_server_names | xargs echo docker stop)"
    ru_info "    unset RU_DEBUG RU_CONF"
  fi
  echo "[${ru_env_name}]" > ${ru_ansible_inventory}
  for ru_server in $(ru_server_nums) ; do
    echo ${ru_servers_names[${ru_server}]}
  done >> ${ru_ansible_inventory}
}

ru_run() {
  local ru_servers=$(ru_server_nums)
  local ru_background_option
  [ "$1" == "-d" ] && ru_background_option="-d" && shift
  [ "$1" == "-n" ] && ru_servers=$(ru_server_num $2) && shift 2
  for ru_server in ${ru_servers}
  do
    local ru_server_name=${ru_servers_names[${ru_server}]}
    docker exec ${ru_background_option} -i ${ru_server_name} "$@"
  done
}

ru_server_name() {
  local ru_server_num=$(ru_server_num $1)
  echo ${ru_servers_names[${ru_server_num}]}
}

ru_server_names() {
  for ru_server in $(ru_server_nums) ; do
    echo ${ru_servers_names[${ru_server}]}
  done
}

# vim: syntax=sh
