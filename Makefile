.PHONY: tests
tests:
	RU_ENV_IMAGE=registry.gitlab.com/role_unit/role_unit_containers:build tests/bash_unit tests/tests_apache2
	# RU_ENV_IMAGE=debian10 tests/bash_unit tests/tests_apache2

tests_check:
	RU_ENV_IMAGE=registry.gitlab.com/role_unit/role_unit_containers:build tests/bash_unit tests/tests_apache2_check_mode
	# RU_ENV_IMAGE=debian10 tests/bash_unit tests/tests_apache2
